# coding=utf-8
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.utils import simplejson
from django import forms

from pyquery import PyQuery as pq

from home.models import Catalog,Item,Tag

import urllib2

class ItemForm(forms.Form):
	name = forms.CharField();
#Ìø×ªºóÌ¨¹ÜÀíÖ÷½çÃæ
def index(req):
	return render_to_response('index.html',{'catalogList':getAllCatalog(),'tagList':getAllTag()})
def index1(req):
	if req.method == 'POST':
		form = ItemForm(req.POST)
		if form.is_valid():
			print form.cleaned_data
			return HttpResponse ('ok')
	else :
		form = ItemForm()
	return render_to_response('index1.html',{'form':form})

#µÃµ½ËùÓÐµÄÀ¸Ä¿
def getAllCatalog():
	catalogList=Catalog.objects.all()
	return catalogList

#µÃµ½ËùÓÐ±êÇ©
def getAllTag():
	tagList=Tag.objects.all()
	return tagList

#½âÎöÍÆ¹ãµØÖ·
def resolveUrl(req,address):
	address=req.get_full_path()
	#address = 'http://redirect.simba.taobao.com/rd?w=unionnojs&f=http%3A%2F%2Fre.taobao.com%2Feauction%3Fe%3DJxPF6UIf47XghojqVNxKsbfXOhk7kGtzHnLSp9l07xyLltG5xFicOSZqewpHPyZzEl%252BVb03J11hUQiDjp6tQAp55ZyQFEp97wR%252Fx62uEIjCB3ujUJI0OeA%253D%253D%26ptype%3D100010&k=e2e107a2b72ca1b1&c=un&b=alimm_0&p=mm_26845928_6126662_21402018'
	address= address.replace('/webmaster/resolveUrl/','')
	print 'address:\n'+address
	html=getHtmlByUrl(address)
	item=resolveHtml(html)
	item["extended_address"]=address # ÍÆ¹ãµØÖ·
	return HttpResponse(simplejson.dumps(item,ensure_ascii=False),mimetype='application/json')

#»ñÈ¡ÍøÒ³html´úÂë
def getHtmlByUrl(url):
	f = urllib2.urlopen('http://redirect.simba.taobao.com/rd?w=unionnojs&f=http%3A%2F%2Fre.taobao.com%2Feauction%3Fe%3DJxPF6UIf47XghojqVNxKsbfXOhk7kGtzHnLSp9l07xyLltG5xFicOSZqewpHPyZzEl%252BVb03J11hUQiDjp6tQAp55ZyQFEp97wR%252Fx62uEIjCB3ujUJI0OeA%253D%253D%26ptype%3D100010&k=e2e107a2b72ca1b1&c=un&b=alimm_0&p=mm_26845928_6126662_21402018')
	html=f.read()
	f.close()
	return html

#½âÎöHTMLÄÚÈÝ£¬µÃµ½ÉÌÆ·ÐÅÏ¢
def resolveHtml(html):
	d = pq(html)
	name=d('h2.featuredTitle').text() #ÉÌÆ·Ãû³Æ
	link_img=d('img.featuredImg').attr('src') #ÉÌÆ·Í¼Æ¬µØÖ·
	link=d('a.btnBuy').attr('href') #È¥¹ºÂòµØÖ·
	price=d('a.priceNum').text() #ÉÌÆ·¼Û¸ñ
	location=d('p.location').text() #ÉÌÆ·ËùÔÚµØ
	salesNum=d('a.salesNum').text() #ÉÌÆ·ÏúÁ¿
	return {'name':name,'link_img':link_img,'link':link,'price':price,'location':location,'salesNum':salesNum}

# ±£´æÉÌÆ·ÐÅÏ¢
import time
def saveItem(request): 
    name = request.REQUEST.get('name','default')
    catalog = request.REQUEST.get('catalog','default')
    link_img = request.REQUEST.get('link_img','default')
    link = request.REQUEST.get('link','default')
    price = request.REQUEST.get('price',1)
    price_old = request.REQUEST.get('price_old',1)
    sale_count = request.REQUEST.get('sale_count',1)
    tags = request.REQUEST.get('tags','default')
    liked = request.REQUEST.get('liked',1)
    clicked = request.REQUEST.get('clicked',1)
    item = Item()
    item.name=name
    item.link_img=link_img
    item.link=link
    item.price=1
    item.price_old=1
    item.sale_count=1
    item.liked=1
    item.clicked=1
    item.put_time=time.time()

    catObj=Catalog.objects.get(id__exact=catalog)
    item.catalog=catObj
    item.save()
    tagObj=Tag.objects.get(id__exact=catalog)
    item.tags.add(tagObj)
    item.save()
    print tagObj
    # item.tags=tagObj
    # item.save()
    # t = Tag()
    # t.name='tag'
    # t.save()
    # c = Catalog()
    # c.name='catalog'
    # c.ename='ename'
    # c.order=100
    # c.save()
    print name+"\n"+catalog+"\n"+link_img+"\n"+link+"\n"+price+"\n"+price_old+"\n"+sale_count+"\n"+tags+"\n"+liked+"\n"+clicked
    return HttpResponse('YOU USE GET METHOD.S')