from django.conf.urls import patterns


urlpatterns = patterns('',
    (r'^$','webmaster.views.index'),
    (r'^index1/$','webmaster.views.index1'),
    (r'^resolveUrl/(?P<address>.+)/$','webmaster.views.resolveUrl'),
    (r'^saveItem','webmaster.views.saveItem'),
)