# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Tag'
        db.create_table('home_tag', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=40)),
        ))
        db.send_create_signal('home', ['Tag'])

        # Adding M2M table for field tags on 'Item'
        m2m_table_name = db.shorten_name('home_item_tags')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('item', models.ForeignKey(orm['home.item'], null=False)),
            ('tag', models.ForeignKey(orm['home.tag'], null=False))
        ))
        db.create_unique(m2m_table_name, ['item_id', 'tag_id'])


    def backwards(self, orm):
        # Deleting model 'Tag'
        db.delete_table('home_tag')

        # Removing M2M table for field tags on 'Item'
        db.delete_table(db.shorten_name('home_item_tags'))


    models = {
        'home.catalog': {
            'Meta': {'ordering': "['order']", 'object_name': 'Catalog'},
            'ename': ('django.db.models.fields.CharField', [], {'default': "'cat'", 'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'order': ('django.db.models.fields.IntegerField', [], {})
        },
        'home.item': {
            'Meta': {'ordering': "['-pub_time']", 'object_name': 'Item'},
            'catalog': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['home.Catalog']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'default': "'#'", 'max_length': '400'}),
            'link_img': ('django.db.models.fields.CharField', [], {'default': "'/static/images/item.jpg'", 'max_length': '400'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'price_old': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'pub_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'sale_cout': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['home.Tag']", 'symmetrical': 'False'})
        },
        'home.tag': {
            'Meta': {'object_name': 'Tag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        }
    }

    complete_apps = ['home']