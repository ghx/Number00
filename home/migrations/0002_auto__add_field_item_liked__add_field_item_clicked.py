# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Item.liked'
        db.add_column('home_item', 'liked',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'Item.clicked'
        db.add_column('home_item', 'clicked',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Item.liked'
        db.delete_column('home_item', 'liked')

        # Deleting field 'Item.clicked'
        db.delete_column('home_item', 'clicked')


    models = {
        'home.catalog': {
            'Meta': {'ordering': "['order']", 'object_name': 'Catalog'},
            'ename': ('django.db.models.fields.CharField', [], {'default': "'cat'", 'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'order': ('django.db.models.fields.IntegerField', [], {})
        },
        'home.item': {
            'Meta': {'ordering': "['-pub_time']", 'object_name': 'Item'},
            'catalog': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['home.Catalog']"}),
            'clicked': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'liked': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'link': ('django.db.models.fields.CharField', [], {'default': "'#'", 'max_length': '400'}),
            'link_img': ('django.db.models.fields.CharField', [], {'default': "'/static/images/item.jpg'", 'max_length': '400'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'price_old': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'pub_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'sale_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['home.Tag']", 'symmetrical': 'False'})
        },
        'home.tag': {
            'Meta': {'object_name': 'Tag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        }
    }

    complete_apps = ['home']