from django.contrib import admin
from models import Catalog,Item,Tag

class CatalogAdmin(admin.ModelAdmin):
	fields = ['name','order']
	list_display = ('name','order')

class ItemAdmin(admin.ModelAdmin):
	list_display = ('name','catalog','get_tags','pub_time')

class TagAdmin(admin.ModelAdmin):
	list_display = ('name','items_count')

admin.site.register(Catalog,CatalogAdmin)
admin.site.register(Item,ItemAdmin)
admin.site.register(Tag,TagAdmin)
