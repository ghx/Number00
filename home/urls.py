from django.conf.urls import patterns

urlpatterns = patterns('',
    (r'^getCat/$','home.views.getCat'),
    (r'^incLike$','home.views.incLike'),
    (r'^incClick$','home.views.incClick'),
)
