﻿from django.db import models

# 商品分类
class Catalog(models.Model):
    # 分类名称
    name = models.CharField(max_length=40)
    # 英文名称用于URL显示,要不相同
    ename = models.CharField(max_length=120,default='cat')
    # 排序ID
    order = models.IntegerField()

    class Meta:
        ordering = ['order']
    
    def __unicode__(self):
        return self.name
# 商品标签
class Tag(models.Model):
	# 标签名称
	name = models.CharField(max_length=40)
	
	def items_count(self):
		return self.item_set.count()
	
	def __unicode__(self):
		return self.name

# 商品
class Item(models.Model):
    # 商品名称
    name = models.CharField(max_length=255)
    # 所属分类
    catalog = models.ForeignKey(Catalog)
    # 展示图片链接
    link_img = models.CharField(max_length=400,default='/static/images/item.jpg')
    # 添加时间
    pub_time = models.DateTimeField(auto_now_add=True)
    # 购买链接
    link = models.CharField(max_length=400,default='#')
    # 商品价格
    price = models.FloatField(default=0)
    # 商品原件
    price_old = models.FloatField(default=0)
    # 销量
    sale_count = models.IntegerField(default=0)
    # 标签
    tags = models.ManyToManyField(Tag)
    # 喜欢数量
    liked = models.IntegerField(default=0)
    # 点击数
    clicked = models.IntegerField(default=0)
    
    class Meta:
        ordering = ['-pub_time']
    
    def get_tags(self):
		rs = []
		for tag in self.tags.all():
		    rs.append(tag.name)
		return ' '.join(rs)
    
    def __unicode__(self):
        return self.name


