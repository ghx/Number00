import os
import sys

root = os.path.dirname(os.path.abspath(__file__))

sp_path = os.path.join(root,'site-packages')

sys.path.insert(0,sp_path)
sys.path.insert(0,os.path.join(sp_path,'pyquery-1.2.8-py2.7.egg'))
sys.path.insert(0,os.path.join(sp_path,'lxml-3.3.5-py2.7-win-amd64.egg'))
sys.path.insert(0,os.path.join(sp_path,'cssselect-0.9.1-py2.7.egg'))

import sae
from apps import wsgi

application = sae.create_wsgi_app(wsgi.application)
