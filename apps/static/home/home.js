// 首页功能模块
$(function() {
    waterFull();
    initEvent();
    initData();
});
//瀑布流

function waterFull() {
    $('#items').masonry({
        columnWidth: 236,
        gutterWidth: 10,
        itemSelector: '.item'
    });
    $('img.lazy').lazyload({
        load: function() {}
    });
}
//初始化首页事件

function initEvent() {
    //菜单显示
    var timer;
    $('.menu-nav').hover(function() {
        clearTimeout(timer);
        timer = setTimeout(function() {
            $('#header_main_menu').fadeIn(300);
        }, 0);
    }, function() {
        clearTimeout(timer);
        timer = setTimeout(function() {
            $('#header_main_menu').fadeOut(300);
        }, 300);
    });
    //搜索框动画
    $('#query').focus(function() {
        clearTimeout(timer);
        timer = setTimeout(function() {
            $('#query').animate({
                width: '160px'
            }, 300);
        }, 0);
    }).blur(function() {
        clearTimeout(timer);
        timer = setTimeout(function() {
            $('#query').animate({
                width: '120px'
            }, 300);
        }, 300);
    });
    //充值框
    $('#phoneCharge').hover(function() {
        var obj = $(this);
        clearTimeout(timer);
        timer = setTimeout(function() {
            obj.animate({
                right: '20px'
            }, 300);
        }, 0);
    }, function() {
        var obj = $(this);
        clearTimeout(timer);
        timer = setTimeout(function() {
            obj.animate({
                right: '-190px'
            }, 600);
        }, 600);
    });
    resize();
    $(window).resize(function() {
        clearTimeout(timer);
        timer = setTimeout(resize, 300);
    });
    itemsEvent();
}
//窗口大小改变事件

function resize() {
    var items_w = $(window).width() - 40;
    var c_item_w = $('.item').first().width();
    var item_w_t = c_item_w + 10;
    var mod = items_w % item_w_t;
    var index = Math.floor(items_w / item_w_t);
    if (mod > 6 && mod < 236) {
        var width = index * item_w_t - 10;
        $('#items').width(width);
        $('#items').masonry();
    } else if (mod >= 236) {
        var width = (index + 1) * item_w_t - 10;
        $('#items').width(width);
        $('#items').masonry();
    }

}
//商品事件
function itemsEvent() {
    $('.btn-like').click(callbackClickLike);
    $('.item a.img').click(callbackItemClick);
}
function callbackItemClick(){
    var id = $(this).parents('.item').attr('data-id');
    $.get("/api/incClick?id="+id,function(data){
        if( data === 'False' ) {
            try{
                console.log(data);
            }catch(e){ }
        }
    });
}
//
function callbackClickLike() {
    var id = $(this).parents('.item').attr('data-id');
    var txt = $(this).children('.text');
    txt.html(parseInt(txt.html()) + 1);
    $.get("/api/incLike?id="+id,function(data){
        if(data === 'False'){
            try{
                console.log(data);
            }catch(e){ }
        }
    });
}

//初始化首页数据

function initData(callback) {
    $.get('/api/getCat', callbackInitCatalog, 'json');
}
//初始化目录

function callbackInitCatalog(data) {
    data = (data !== null) ? $.parseJSON(data) : [];
    var lis = ['<li><a href="/" title="所有类型">所有类型</a></li>'];
    for (var i = 0; i < data.length; i++) {
        var c = data[i];
        lis.push('<li><a href="/cat/' + c.fields.ename + '" title="' + c.fields.name + '">' + c.fields.name + '</a></li>');
    }
    $('.cat_menu').html(lis.join(''));
}