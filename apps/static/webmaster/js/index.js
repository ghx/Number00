function resolveUrl(obj){
    $.get('/webmaster/resolveUrl/'+obj.value+'/',backResolveUrl,'json');
}
function backResolveUrl(data){
    var mainTable = '.formtable';
    $(mainTable+" input[name='name']").val(data.name);
    $(mainTable+" input[name='price']").val(data.price);
    $(mainTable+" input[name='link']").val(data.link);
    $(mainTable+" input[name='extended_address']").val(data.extended_address);
    $(mainTable+" input[name='location']").val(data.location);
    $(mainTable+" input[name='salesNum']").val(data.salesNum);
    $(mainTable+" input[name='link_img']").val(data.link_img);
    $(mainTable+" img[class='link_img']").attr('src',data.link_img);
}
/** 提交商品表单 **/
function submitItem(formId){
    var formJson=UtilFormToJson.formToJson(formId);
    var itemData = formJson.join("&").replace(/\,\,/gm,",");
    $.ajax({
        type: "POST",
        data: itemData,
        url: "/webmaster/saveItem",
        dataType: "json",
        cache: false,
        success: function(data){
            alert('success:'+data)
        },        
        error: function(data){
            alert('error:'+data)
        }
    }); 
}