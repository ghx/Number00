var UtilBase={
	/** 
	* json对象转字符串形式 
	*/ 
	json2str:function(o) { 
		var arr = []; 
		var fmt = function(s) { 
			if (typeof s == 'object' && s != null) return UtilBase.json2str(s); 
			return /^(string|number)$/.test(typeof s) ? "'" + s + "'" : s; 
		} 
		for (var i in o) arr.push("'" + i + "':" + fmt(o[i])); 
		return '{' + arr.join(',') + '}'; 
	}
}

var UtilFormToJson={
	/** form 表单 转换成json **/
	formToJson:function(formId){
	    var formJson=[];
	    $("#"+formId+" input[type='text']").each(function(){
	        var item=UtilFormToJson.getKeyValueByType($(this));
	        if(item){
	            formJson.push(item);
	        }        
	    });
	    var m2mJsonObj=eval('('+'{}'+')');
	    $("#"+formId+" input[type='checkbox']").filter(":checked").each(function(){
	        var item=UtilFormToJson.getKeyValueByType($(this)); 
	        var name=$(this).attr("name");
	        if(item){
	            if(typeof m2mJsonObj[name]=="undefined"){
	                m2mJsonObj[name]=[item];
	            }else{
	                m2mJsonObj[name].push(item);
	            }
	        }
	    });
	    var jsonObjStr=UtilBase.json2str(m2mJsonObj);
	    jsonObjStr=jsonObjStr.substring(1,jsonObjStr.length-1);
	    var splitIndex=jsonObjStr.indexOf(":");
	    formJson.push(jsonObjStr.substring(1,splitIndex-1)+"="+jsonObjStr.substring(splitIndex+1));
	    $("#"+formId+" select").each(function(){
	        var item=UtilFormToJson.getKeyValueByType($(this));
	        if(item){
	            formJson.push(item);
	        }        
	    });
	    return formJson;
	},
	/** switch input type **/
	getKeyValueByType:function(jqueryObj){
	    var fieldType=jqueryObj.attr("class");
	    switch(fieldType){
	        case 'charfield':return UtilFormToJson.getCharfield(jqueryObj);break;
	        case 'integerfield':return UtilFormToJson.getIntegerfield(jqueryObj);break;
	        case 'floatfield':return UtilFormToJson.getFloatfield(jqueryObj);break;
	        case 'manytomany':return UtilFormToJson.getCheckboxData(jqueryObj);break;
	        case 'manytoone':return UtilFormToJson.getSelectData(jqueryObj);break;
	        default :return "";break;
	    }        
	},
	/** 获取字符串类型的数据 **/
	getCharfield:function(jqueryObj){
	    var name=jqueryObj.attr("name");
	    var value=jqueryObj.val();
	    return name+"="+value;
	},
	/** 获取整形类型的数据 **/
	getIntegerfield:function(jqueryObj){
	    var name=jqueryObj.attr("name");
	    var value=jqueryObj.val();
	    return name+"="+value;
	},
	/** 获取浮点形类型的数据 **/
	getFloatfield:function(jqueryObj){
	    var name=jqueryObj.attr("name");
	    var value=jqueryObj.val();
	    return name+"="+value;
	},
	/** 获取 select 中的数据 **/
	getSelectData:function(jqueryObj){
	    var name=jqueryObj.attr("name");
	    var value=jqueryObj.val();
	    return name+"="+value;
	},
	/** 获取 checkbox 中的数据 **/
	getCheckboxData:function(jqueryObj){
	    var name=jqueryObj.attr("name");
	    var value=jqueryObj.val();
	    return value;
	}
}