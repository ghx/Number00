﻿from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
from django.contrib import admin
admin.autodiscover()

#网站首页
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'apps.views.home', name='home'),
    # url(r'^apps/', include('apps.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^$','home.views.index',name='home'),
    #url(r'^/(?P<page>\d*)$','home.views.index',name='home-page'),
    url(r'^cat/(?P<ename>\w+)$','home.views.cat'),
    url(r'^api/',include('home.urls')),
    # url(r'^home/', include('home.urls')),
    # url(r'^siteadmin/',include('admin.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^webmaster/', include('webmaster.urls')),
)

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns += staticfiles_urlpatterns()
